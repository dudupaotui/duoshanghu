<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
   </head>
   <script>
    function cashout(id,storeid,moneycount){
	   Plugins.confirm({title:'信息提示',content:'确定结算吗？',okText:'确定',cancelText:'取消',okFun:function(){
		   Plugins.closeWindow();
		   Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
		   $.post("<?php echo U('Admin/Yunorder/cashout');?>",{id:id,storeid:storeid,moneycount:moneycount},function(data,textStatus){
					var json = WST.toJson(data);console.log(json);
					if(json.status=='1'){
						Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
						   location.reload();
						}});
					}else{
						Plugins.closeWindow();
						Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
					}
				});
	   }});
   }
   </script>
   <body class='wst-page'>
 
 		<p style="font-size:16px;padding:10px;">云购所有订单总金额：<?php echo ($AlltotalMoney); ?>元，<?php if($storeid): echo ($storeName); ?>订单总金额：<?php echo ($totalMoney); ?>元，<?php endif; ?>已结算：<?php echo ($calTotalMoney); ?>元，未结算：<?php echo ($Nosettlement); ?>元</p>
 
       <div class='wst-body'>

		
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='180'>订单号</th>
               <th width='180'>商品名称</th>
			   <th width='100'>所属店铺</th>
               <th width='100'>购买用户</th>
               <th width='100'>购买次数</th>
               <th width='100'>购买总价</th>
               <th width='100'>购买日期</th>
               <th width='130'>是否付款</th>
			   <th width='130'>是否结算</th>
               <th width='100'>管理</th>
             </tr>
           </thead>
           <tbody>
            <?php if(is_array($lists)): $i = 0; $__LIST__ = $lists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
               <td><?php echo ($vo['code']); ?></td>
               <td><?php echo ($vo['shopname']); ?></td>
			    <td><a href="<?php echo U('Yunorder/ordermoney',array('storeid' => $vo['storeid']));?>"><?php echo ($vo['storeName']); ?></a></td>
               <td><?php echo ($vo['username']); ?></td>
               <td><?php echo ($vo['gonumber']); ?>人次</td>
               <td><?php echo ($vo['moneycount']); ?>元</td>
               <td><?php echo date('Y-m-d H:i:s',$vo['time']);?></td>
               <td>
              <?php if($vo['is_pay'] == 1): ?>已付款
				<?php else: ?>
				未付款<?php endif; ?>
			  
               </td>
			   <td><?php if($vo['is_cashout'] == 1): ?>已结算
				<?php else: ?>
				未结算<?php endif; ?></td>
               <td>
				    <button type="button" class="btn btn-default glyphicon glyphicon-pencil" onclick="javascript:cashout(<?php echo ($vo['id']); ?>,<?php echo ($vo['storeid']); ?>,<?php echo ($vo['moneycount']); ?>);" >结算</button>&nbsp;
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
                <td colspan='10' align='center'><?php echo ($page); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
</html>