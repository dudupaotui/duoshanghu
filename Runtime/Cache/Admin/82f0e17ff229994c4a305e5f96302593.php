<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>General Control</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS (Via CDN) -->
<!--link rel='stylesheet' type='text/css' href='css/googlefonts.css'-->

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="/Public/js/login/vendor/bootstrap/css/bootstrap.min.css">


<!-- Theme CSS -->
<link rel="stylesheet" type="text/css" href="/Public/js/login/css/vendor.css">
<link rel="stylesheet" type="text/css" href="/Public/js/login/css/theme.css">
<link rel="stylesheet" type="text/css" href="/Public/js/login/css/utility.css">
<link rel="stylesheet" type="text/css" href="/Public/js/login/css/custom.css">
<link rel="stylesheet" href="/Public/js/login/lib/layui/css/layui.css">


<!-- Favicon -->
<link rel="shortcut icon" href="__STATIC_JS__login/img/favicon.ico">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
</head>
<script>
 
   function login(){
	   var params = {};
	   params.loginName = $.trim($('#loginName').val());
	   params.loginPwd = $.trim($('#loginPwd').val());
	   params.id = $('#id').val();
	   if(params.loginName==''){
		   Plugins.Tips({title:'信息提示',icon:'error',content:'请输入账号!',timeout:1000});
		   $('#loginName').focus();
		   return;
	   }
	   if(params.loginPwd==''){
		   Plugins.Tips({title:'信息提示',icon:'error',content:'请输入密码!',timeout:1000});
		   $('#loginPwd').focus();
		   return;
	   }
	   Plugins.waitTips({title:'信息提示',content:'正在登录，请稍后...'});
		$.post("<?php echo U('Admin/index/login');?>",params,function(data,textStatus){
			var json = WST.toJson(data);
			if(json.status=='1'){
				Plugins.setWaitTipsMsg({ content:'登录成功',timeout:1000,callback:function(){
					location.href='<?php echo U("Admin/Index/index");?>';
				}});
			}else{
				Plugins.closeWindow();
				Plugins.Tips({title:'信息提示',icon:'error',content:'账号或密码错误!',timeout:1000});
			}
		});
   }
   
   </script>

<body class="minimal login-page">
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script> 

<!-- Start: Main -->

<div id="main">
  <div id="content">
    <div class="row">
      <div id="page-logo"> <img src="/Public/js/login/img/logos/logo-white.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
    <form class="layui-form" action="" method="post" id="form1">
      <div class="panel-bg">
        <div class="panel">
          <div class="panel-heading"> <span class="panel-title"> <span class="glyphicon glyphicon-lock text-purple2"></span> Login </span> <span class="panel-header-menu pull-right mr15 text-muted fs12"></span> </div>
          <div class="panel-body">
            <div class="login-avatar"> <img src="/Public/js/login/img/avatars/login.png" width="150" height="112" alt="avatar"> </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span>
                <input type="text" class="form-control"  name='loginName' id='loginName'  placeholder="account">             

              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-link"></span> </span>
                <input type="password" name='loginPwd' id='loginPwd' class="form-control" placeholder="password">
              </div>
            </div>
          </div>
          <div class="panel-footer"> <span class="text-muted fs12 lh30"></span> <a class="btn btn-sm bg-purple2 pull-right" href="javascript:login()"><i class="fa fa-home"></i> check in</a>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- End: Main -->

<div class="overlay-black"></div>

<!-- Google Map API --> 
<!--script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script--> 

<!-- jQuery --> 

<script type="text/javascript" src="/Public/js/login/vendor/jquery/jquery_ui/jquery-ui.min.js"></script> <!-- Bootstrap --> 

<!-- Page Plugins --> 
<script type="text/javascript" src="/Public/js/login/vendor/plugins/backstretch/jquery.backstretch.min.js"></script> 

<!-- Theme Javascript --> 
<script type="text/javascript" src="/Public/js/login/js/utility/spin.min.js"></script> 
<script type="text/javascript" src="/Public/js/login/js/utility/underscore-min.js"></script> 
<script type="text/javascript" src="/Public/js/login/js/main.js"></script> 
<script type="text/javascript" src="/Public/js/login/js/ajax.js"></script> 
<script type="text/javascript" src="/Public/js/login/js/custom.js"></script> 
<script src="/Public/js/login/lib/layui/layui.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
	  
	 "use strict";

     // Init Theme Core 	  
     //Core.init();

     // Enable Ajax Loading 	  
     //Ajax.init();
	 
	 // Init Full Page BG(Backstretch) plugin
  	 $.backstretch("/Public/js/login/img/stock/splash/2.jpg");



});
            
</script>
</body>
</html>