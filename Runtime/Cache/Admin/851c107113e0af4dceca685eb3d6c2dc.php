<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
      <style type="text/css">
		#preview{border:1px solid #cccccc; background:#CCC;color:#fff; padding:5px; display:none; position:absolute;}
	  </style>
   </head>
   <script>
    //修改帖子状态
    function toggleStatus(t,v){
		Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
		$.post("<?php echo U('Admin/Posts/editStatus');?>",{id:v,status:t},function(data,textStatus){
			var json = WST.toJson(data);
			if(json.status=='0'){
				Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
				    location.reload();
			}});
			}else{
				Plugins.closeWindow();
				Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
			}
     	});
    }
    //删除单个帖子
    function delPosts(id){
    	var id = id;
    	Plugins.confirm({title:'信息提示',content:'您确定要删除该帖子吗?',okText:'确定',cancelText:'取消',okFun:function(){
			Plugins.closeWindow();
			Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			$.post("<?php echo U('Admin/Posts/delPosts');?>",{id:id},function(data,textStatus){
				var json = WST.toJson(data);
				if(json.status=='0'){
					Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
					    location.reload();
				}});
				}else{
					Plugins.closeWindow();
					Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
				}
	     	});
		}});
    }
    //批量删除帖子
    function BatchDelete(){
      var ids = [];
      $('.chk').each(function(){
          if($(this).prop('checked'))ids.push($(this).val());
      })
      Plugins.confirm({title:'信息提示',content:'您确定要删除所选帖子吗?',okText:'确定',cancelText:'取消',okFun:function(){
          Plugins.closeWindow();
          Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
          $.post("<?php echo U('Admin/Posts/delSelectPosts');?>",{id:ids.join(',')},function(data,textStatus){
              var json = WST.toJson(data);
              if(json.status=='0'){
                  Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
                      location.reload();
                  }});
              }else{
                  Plugins.closeWindow();
        Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
              }
          });
      }});
    }
    //批量隐藏帖子
    function hideSelectPosts(){
      var ids = [];
      $('.chk').each(function(){
          if($(this).prop('checked'))ids.push($(this).val());
      })
      Plugins.confirm({title:'信息提示',content:'您确定要隐藏所选帖子吗?',okText:'确定',cancelText:'取消',okFun:function(){
          Plugins.closeWindow();
          Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
          $.post("<?php echo U('Admin/Posts/hideSelectPosts');?>",{id:ids.join(',')},function(data,textStatus){
              var json = WST.toJson(data);
              if(json.status=='0'){
                  Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
                      location.reload();
                  }});
              }else{
                  Plugins.closeWindow();
        Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
              }
          });
      }});
    }
    //批量显示帖子
    function showSelectPosts(){
      var ids = [];
      $('.chk').each(function(){
          if($(this).prop('checked'))ids.push($(this).val());
      })
      Plugins.confirm({title:'信息提示',content:'您确定要显示所选帖子吗?',okText:'确定',cancelText:'取消',okFun:function(){
          Plugins.closeWindow();
          Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
          $.post("<?php echo U('Admin/Posts/showSelectPosts');?>",{id:ids.join(',')},function(data,textStatus){
              var json = WST.toJson(data);
              if(json.status=='0'){
                  Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
                      location.reload();
                  }});
              }else{
                  Plugins.closeWindow();
        Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
              }
          });
      }});
    }
   $.fn.imagePreview = function(options){
		var defaults = {}; 
		var opts = $.extend(defaults, options);
		var t = this;
		xOffset = 5;
		yOffset = 20;
		if(!$('#preview')[0])$("body").append("<div id='preview'><img width='200' src=''/></div>");
		$(this).hover(function(e){
			   $('#preview img').attr('src',"/"+$(this).attr('img'));      
			   $("#preview").css("top",(e.pageY - xOffset) + "px").css("left",(e.pageX + yOffset) + "px").show();      
		  },
		  function(){
			$("#preview").hide();
		}); 
		$(this).mousemove(function(e){
			   $("#preview").css("top",(e.pageY - xOffset) + "px").css("left",(e.pageX + yOffset) + "px");
		});
	}
  $(document).ready(function(){
	    $('.imgPreview').imagePreview();
	    //全选
        $("#selected").click(function(){
            $('input[num="list"]').prop('checked',true);
            $('#cancel').attr('checked',false);
        });
        //取消全选
        $("#cancel").click(function(){
            $('input[num="list"]').prop('checked',false);
            $('#selected').attr('checked',false);
        });
  });
   </script>
   <body class='wst-page'>
    <form method='post' action='<?php echo U("Admin/Posts/postList");?>'>
   		<div class='wst-tbar'> 
          发布人：<input type='text' id='userName' name='userName' value='<?php echo ($userName); ?>'/>            
	        标题：<input type='text' id='title' name='title' value='<?php echo ($title); ?>'/>          
	        内容：<input type='text' id='content' name='content' value='<?php echo ($content); ?>'/>  
	        状态：<select name="status">
					 <option  value="-1" <?php if($status == -1): ?>selected="selected"<?php endif; ?>>请选择</option>
					 <option  value="1" <?php if($status == 1): ?>selected="selected"<?php endif; ?>>显示</option>
					 <option  value="0" <?php if($status == 0): ?>selected="selected"<?php endif; ?>>隐藏</option>
					</select>
          分类：<select name="cid">
           <option  value="-1" <?php if($cid == -1): ?>selected="selected"<?php endif; ?>>请选择</option>
           <?php if(is_array($Page['postCate'])): $i = 0; $__LIST__ = $Page['postCate'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option  value="<?php echo ($vo['cid']); ?>" <?php if($cid == $vo['cid']): ?>selected="selected"<?php endif; ?>><?php echo ($vo['name']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
          </select>  
	  		<button type="submit" class="btn btn-primary glyphicon glyphicon-search">查询</button> 
       	</div>
    </form>
       <div class='wst-body'>
        <table class="table table-hover table-striped table-bordered wst-list" style="table-layout: fixed;word-wrap:break-word">
           <thead>
             <tr>
               <th width='6%'><input type='checkbox' name='chk' onclick='javascript:WST.checkChks(this,".chk")'/>序号</th>
               <th width='12%'>分类</th>
               <th width='12%'>标题</th>
               <th width='50%'>内容</th>
               <th width='8%'>发布人</th>
               <th width='10%'>发布时间</th>
               <th width='6%'>状态</th>
               <th width='12%'>操作</th>
             </tr>
           </thead>
           <tbody>
            <?php if(is_array($Page['root'])): $i = 0; $__LIST__ = $Page['root'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
               <td><input type='checkbox' class='chk' name='chk_<?php echo ($vo['id']); ?>' value='<?php echo ($vo['id']); ?>'/><?php echo ($i); ?></td>
               <td><?php echo ($vo['cateName']); ?></td>
               <td><?php echo ($vo['title']); ?></td>
               <td><?php echo ($vo['content']); ?></td>
               <td><?php echo ($vo['userName']); ?></td>
               <td><?php echo ($vo['ftime']); ?></td>
               <td>
                    <div class="dropdown">
                    <?php if($vo['status']==0 ): ?><button class="btn btn-danger dropdown-toggle wst-btn-dropdown"  type="button" data-toggle="dropdown">隐藏<span class="caret"></span>
                    </button>
                    <?php else: ?>
                      <button class="btn btn-success dropdown-toggle wst-btn-dropdown" type="button" data-toggle="dropdown">显示<span class="caret"></span>
                    </button><?php endif; ?>
                      <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleStatus(1,<?php echo ($vo['id']); ?>)">显示</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleStatus(0,<?php echo ($vo['id']); ?>)">隐藏</a></li>
                     </ul>
                    </div>
               </td>
               <td>
               <a class="btn btn-primary glyphicon" href='<?php echo U("Admin/Posts/toDetail",array("id"=>$vo["id"]));?>'>查看</a> 
               <?php if(in_array('spsh_04',$WST_STAFF['grant'])){ ?>
               <button type="button" class="btn btn-danger" onclick="javascript:delPosts(<?php echo ($vo['id']); ?>)">删除</button>
               <?php } ?>
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
		       <tr>
		            <td colspan='9' style="background:none;;">
		                <input type='checkbox' name='chk' onclick='javascript:WST.checkChks(this,".chk")'/>全选/取消
		                <input type="button" onclick="javascript:BatchDelete()" value="批量删除"/>
	                    <input type="button" onclick="javascript:hideSelectPosts()" value="批量隐藏"/>
	                    <input type="button" onclick="javascript:showSelectPosts()" value="批量显示"/>
		            </td>
		          </tr>
             <tr>
                <td colspan='9' align='center'><?php echo ($Page['pager']); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
</html>