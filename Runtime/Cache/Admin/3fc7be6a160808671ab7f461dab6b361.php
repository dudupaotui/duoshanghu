<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
      <link rel="stylesheet" href="/Tpl/Admin/css/layer.css">
      <script type="text/javascript" src="/Tpl/Admin/js/layer.js"></script>
      <script type="text/javascript" src="/Tpl/Admin/js/layer.ext.js"></script>
   </head>
   <script>

    $(function(){

     layer.config({
        extend: 'extend/layer.ext.js'
      });

    var val_r=<?php echo ($return); ?>;
    switch(val_r){
      case 1: 
        layer.msg('发布成功', {icon: 1});
        break;
      case 2: 
         layer.msg('修改成功', {icon: 1});
        break;
      case 3: 
        layer.msg('操作失败',{icon:5});
        break;
    }
  })
   </script>
   <script>
   function del(id){
     Plugins.confirm({title:'信息提示',content:'您确定要删除该优惠券吗?',okText:'确定',cancelText:'取消',okFun:function(){
       Plugins.closeWindow();
       Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
       $.post("<?php echo U('Admin/Youhui/del');?>",{id:id},function(data,textStatus){
          var json = WST.toJson(data);
          if(json.status=='1'){
            Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
              location.reload();
            }});
          }else{
            Plugins.closeWindow();
            parent.showMsg({msg:'操作失败!',status:'danger'});
          }
        });
        }});
   }
   </script>
   <body class='wst-page'>
     <form method='post' action="<?php echo U('Admin/Youhui/index');?>">
       <div class='wst-tbar'> 
       优惠券名称：<input type='text' id='youhuiName' name='youhuiName' class='form-control wst-ipt-10' value='<?php echo ($youhuiName); ?>'/> 
  <button type="submit" class="btn btn-primary glyphicon glyphicon-search">查询</button> 
 <!--  <a class="btn btn-success glyphicon glyphicon-plus" href="<?php echo U('Admin/Youhui/add');?>" style='float:right'>新增</a> -->

       </div>
       </form>
       <div class='wst-body'>
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='30'>序号</th>
               <th width='100'>优惠券名称</th>
               <!-- <th width='30'>可用城市</th> -->
               <th width='30'>优惠券总条数</th>
               <th width='30'>折扣/减免额度</th>
               <th width='30'>需消费金额</th>
               <th width='30'>总领取量</th>
               <th width='80'>优惠券范围</th>
               <th width='80'>创建时间</th>
               <th width='80'>结束时间</th>
               <th width='30'>状态</th>
               <th width='120'>操作</th>
             </tr>
           </thead>
           <tbody>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
             <td><?php echo ($vo['id']); ?></td>
             <td><?php echo ($vo['name']); ?></td>
             <!-- <td><?php echo ($vo['city_id']); ?></td> -->
             <td><?php echo ($vo['total_num']); ?></td>
             <td><?php echo ($vo['breaks_menoy']); switch($vo['youhui_type']): case "0": ?>元<?php break;?>
              <?php case "1": ?>折<?php break; endswitch;?></td>
             <td><?php echo ($vo['total_fee']); ?>元</td>
             <td><?php echo ($vo['user_count']); ?></td>
             <td><?php switch($vo['youhui_scope']): case "1": ?>全部商品<?php break;?>
             <?php case "2": ?>部分商户分类<?php break;?>
             <?php case "3": ?>商品<?php break;?>
             <?php case "4": ?>品牌<?php break;?>
             <?php case "5": ?>部分商城分类<?php break; endswitch;?>
             </td>
             <td><?php echo ($vo['create_time']); ?></td>
             <td><?php echo ($vo['end_time']); ?></td>
             <td>
             <?php switch($vo['is_effect']): case "0": ?>无效<?php break;?>
             <?php case "1": ?>有效<?php break;?>
             <?php case "2": ?>已领完<?php break; endswitch;?></td>
             <td> 
               <a class="btn btn-default glyphicon glyphicon-pencil" href="<?php echo U('Admin/Youhui/updata',array('id'=>$vo['id']));?>">修改</a>&nbsp;
               <button type="button" class="btn btn-default glyphicon glyphicon-trash" onclick="javascript:del(<?php echo ($vo['id']); ?>)">刪除</buttona>
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
               <td colspan='12' align="center"><?php echo ($show); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
</html>