<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
       <script type="text/javascript" src="/Public/js/jedate/jedate.js"></script>
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
     
      <script src="/Public/plugins/plugins/plugins.js"></script>
         <script src="/Public/js/layer.js"></script>
      <style>
	.num,.current{padding:10px;display:inline-block;}
.isShow,.del{cursor:pointer;}
	</style>
   </head>
   <script>
		//修改评论状态
	    function toggleStatus(t,v){
			Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			$.post("<?php echo U('Admin/Posts/editCommentStatus');?>",{id:v,status:t},function(data,textStatus){
				var json = WST.toJson(data);
				if(json.status=='0'){
					Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
					    location.reload();
				}});
				}else{
					Plugins.closeWindow();
					Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
				}
	     	});
	    }
	   function del(id){
		   Plugins.confirm({title:'信息提示',content:'您确定要删除该广告吗?',okText:'确定',cancelText:'取消',okFun:function(){
			   Plugins.closeWindow();
			   Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			   $.post("<?php echo U('Admin/Ads/del');?>",{id:id},function(data,textStatus){
						var json = WST.toJson(data);
						if(json.status=='1'){
	                        Plugins.setWaitTipsMsg({ content:'操作成功',timeout:1000,callback:function(){
	                            location.href='<?php echo U("Admin/Ads/index");?>';
	                        }});
						}else{
							Plugins.closeWindow();
							Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
						}
					});
		   }});
	   }
   	    //全选和取消
	    $(document).ready(function(){
		    //全选
	        $("#selected").click(function(){
	            $('input[num="list"]').prop('checked',true);
	            $('#cancel').attr('checked',false);
	        });
	        //取消全选
	        $("#cancel").click(function(){
	            $('input[num="list"]').prop('checked',false);
	            $('#selected').attr('checked',false);
	        });
  		});
   </script>
   <body class='wst-page'>
   	<div style="padding-top: 6px;">
       <div class='wst-tbar'>
       <form method='post' action='<?php echo U("Admin/Posts/postComment");?>' autocomplete="off">
   		评论人：<input type='text' id='userName' name='userName' value='<?php echo ($userName); ?>'/>     
		帖子标题：<input type='text' id='title' name='title' value='<?php echo ($title); ?>'/>          
	    评论内容：<input type='text' id='content' name='content' value='<?php echo ($content); ?>'/>  
        状态：<select name="status">
		<option  value="-1" <?php if($status == -1): ?>selected="selected"<?php endif; ?>>请选择</option>
		<option  value="1" <?php if($status == 1): ?>selected="selected"<?php endif; ?>>显示</option>
		<option  value="0" <?php if($status == 0): ?>selected="selected"<?php endif; ?>>隐藏</option>
		</select>  
  		<button type="submit" class="btn btn-primary glyphicon glyphicon-search" style="display:inline-block">查询</button> 
		  </form>
       	</div>
       </div>
       <div class="wst-body"> 
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='50'><input type='checkbox' name='chk' onclick='javascript:WST.checkChks(this,".chk")'/>序号</th>
               <th width='60'>评论ID</th>
               <th width='70'>评论人</th>
               <th width='60'>帖子ID</th>
               <th width='120'>帖子标题</th>
               <th width='30'>楼层</th>
               <th width='150'>评论内容</th>
               <th width='70'>回复对象</th>
               <th width="70">评论时间</th>
               <th width='45'>状态</th>
               <th width='85'>操作</th>
             </tr>
           </thead>
           <tbody>
           <?php if(is_array($info["root"])): $i = 0; $__LIST__ = $info["root"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$in): $mod = ($i % 2 );++$i;?><tr id="tr_<?php echo ($in["id"]); ?>">
               <td ><input type='checkbox' class='chk' name='chk_<?php echo ($in['id']); ?>' value='<?php echo ($in['id']); ?>'/><?php echo ($i); ?></td>
               <td ><?php echo ($in["id"]); ?></td>
               <td ><?php echo ($in["userName"]); ?></td>
               <td ><?php echo ($in["pid"]); ?></td>
               <td ><?php echo ($in["title"]); ?></td>
               <td ><?php echo ($in["floor"]); ?></td>
               <td style="word-break: break-all;"><?php echo ($in["content"]); ?></td>
               <td><?php echo ($in["objectUserName"]); ?></td>
               <td><?php echo ($in["time"]); ?></td>
               <td>
				    <div class="dropdown">
                    <?php if($in['status']==0 ): ?><button class="btn btn-danger dropdown-toggle wst-btn-dropdown"  type="button" data-toggle="dropdown">隐藏<span class="caret"></span>
                    </button>
                    <?php else: ?>
                      <button class="btn btn-success dropdown-toggle wst-btn-dropdown" type="button" data-toggle="dropdown">显示<span class="caret"></span>
                    </button><?php endif; ?>
                      <ul class="dropdown-menu" role="menu">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleStatus(1,<?php echo ($in['id']); ?>)">显示</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleStatus(0,<?php echo ($in['id']); ?>)">隐藏</a></li>
                     </ul>
                    </div>
               </td>
               <td >
               <a class="btn btn-primary glyphicon" href="<?php echo U('Posts/toDetail',array('id'=>$in['psId']));?>">查看</a>&nbsp;<button type="button" class="btn btn-danger del" data-id="<?php echo ($in["id"]); ?>">删除</button>
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
                <td colspan='11'>
                <input type='checkbox' name='chk' onclick='javascript:WST.checkChks(this,".chk")'/>全选/取消
		        <input type="button" class="delAll btns" value="批量删除">
		        <input type="button" class="showAll btns" value="批量显示">
		        <input type="button" class="hideAll btns" value="批量隐藏"></td>
             </tr>
             <tr>
                <td colspan='11' align='center'><?php echo ($info["pager"]); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
   </body>
   <script>
	   
	   //批量删除所选评论记录
	   $('body').on('click','.delAll',function(){
		   var ids = [];
		   $('.chk').each(function(){
			   if($(this).prop('checked'))ids.push($(this).val());
		   })
		   if(ids.length == 0){
	 		   Plugins.Tips({title:'信息提示',icon:'error',content:'请选择要删除的评论记录!',timeout:1000});
	  		   return;
		   }
       Plugins.confirm({title:'信息提示',content:'您确定要删除所选评论记录吗?',okText:'确定',cancelText:'取消',okFun:function(){
            Plugins.closeWindow();
            Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			   $.ajax({
					  type: "POST",
					  url: "<?php echo U('Posts/delSelectComment');?>",
					  data: {
					      id:ids.join(',')
					  },
					  dataType: "json",
					  success: function(data){
						  if(data.status==0){
			                Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
			                        location.reload();
			                    }});
						  }else{
	                    Plugins.closeWindow();
						Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
						  }
					  }
					});
		}})
	   })
	   //批量隐藏
	   $('body').on('click','.hideAll',function(){
		   var ids = [];
		   $('.chk').each(function(){
			   if($(this).prop('checked'))ids.push($(this).val());
		   })
		   if(ids.length == 0){
	 		   Plugins.Tips({title:'信息提示',icon:'error',content:'请选择要隐藏的评论记录!',timeout:1000});
	  		   return;
		   }
        Plugins.confirm({title:'信息提示',content:'您确定使所选评论为隐藏状态吗?',okText:'确定',cancelText:'取消',okFun:function(){
            Plugins.closeWindow();
            Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			   $.ajax({
				  type: "POST",
				  url: "<?php echo U('Posts/hideSelectComment');?>",
				  data: {
				      id:ids.join(',')
				  },
				  dataType: "json",
				  success: function(data){
					  if(data.status==0){
		                    Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
		                        location.reload();
		                    }});
					  }else{
                    Plugins.closeWindow();
          Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
					  }
				  }
				});
			 }});
	   })
	   //批量显示
	   $('body').on('click','.showAll',function(){
		   var ids = [];
		   $('.chk').each(function(){
			   if($(this).prop('checked'))ids.push($(this).val());
		   })
		   if(ids.length == 0){
	 		   Plugins.Tips({title:'信息提示',icon:'error',content:'请选择要显示的评论记录!',timeout:1000});
	  		   return;
		   }
        Plugins.confirm({title:'信息提示',content:'您确定使所选帖子评论为显示状态吗?',okText:'确定',cancelText:'取消',okFun:function(){
            Plugins.closeWindow();
            Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			   $.ajax({
					  type: "POST",
					  url: "<?php echo U('Posts/showSelectComment');?>",
					  data: {
					      id:ids.join(',')
					  },
					  dataType: "json",
					  success: function(data){
						  if(data.status==0){
		                    Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
		                        location.reload();
		                    }});
						  }else{
                    Plugins.closeWindow();
          Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
						  }
					  }
					});
			 }});
	   })
	   //删除单个评论记录
	   $('body').on('click','.del',function(){
		   var id=$(this).attr('data-id');
	   Plugins.confirm({title:'信息提示',content:'您确定要删除该评论记录吗?',okText:'确定',cancelText:'取消',okFun:function(){
		   Plugins.closeWindow();
		   Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
			   $.ajax({
					  type: "POST",
					  url: "<?php echo U('Posts/delComment');?>",
					  data: {
					      id:id
					  },
					  dataType: "json",
					  success: function(data){
						  if(data.status==0){
							Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
						    	location.reload();
							}});
						  }else{
							Plugins.closeWindow();
							Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
						  }
					  }
					});
			}})
	   })
	   
	   $('.selectAll').on('click',function(){
		   var status=$(this).attr('data-status');
		   if(status==0){
			   $(this).attr('data-status',1);
			   $('.delid').each(function(){
				   $(this).prop('checked',true);
				   })
		   }else{
			   $(this).attr('data-status',0);
			   $('.delid').each(function(){
				   $(this).prop('checked',false);
				   })
		   }
	   })
	   //显示隐藏评论
	   $('body').on('dblclick','.isShow',function(){
		   var _this=$(this);
		   var text=$(this).text();
		   var status=0;
		   if(text=='否'){
			   status=1;
			   text="是";
		   }else{
			   status=0;
			   text="否";
		   }
		   var id=$(this).attr('data-id');
		   $.ajax({
				  type: "POST",
				  url: "<?php echo U('Posts/showHideComment');?>",
				  data: {
				      id:id,
				      status:status
				  },
				  dataType: "json",
				  success: function(data){
					  if(data.status==0){
						  layer.msg('操作成功');
						  $(_this).text(text);
						  
					  }else{
						  layer.msg('操作失败');
					  }
				  }
				});
	   })
	
   
   </script>
</html>